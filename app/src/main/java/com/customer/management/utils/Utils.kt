package com.customer.management.utils

import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.ContentLoadingProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.customer.management.R


class Utils {
    companion object{
        fun logException(className: Class<*>, method: String, e: Exception) {
            Log.e(className.simpleName + "->" + method + " Exception: ", e.message.toString())
            //logCrashLytics(e.message)
        }
        fun makeToast(context: Context,toastMsg:String) {
            val toast = Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT)
            toast.show()
        }
        fun makeDebugToast(context: Context,toastMsg:String) {
            val toast = Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT)
            toast.show()
        }

        fun View.hideKeyboard() {
            val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(windowToken, 0)
        }

        fun  isValidPhoneNumber(number:String): Boolean {
            return !(number.startsWith("0")||number.startsWith("1")||number.startsWith("2")||number.startsWith("3")||number.startsWith("4")||number.startsWith("5"))
        }
//        fun showRecyclerView(recyclerView:RecyclerView, loadingErrorLayout:FrameLayout){
//            loadingErrorLayout.visibility = View.GONE
//            recyclerView.visibility = View.VISIBLE
//        }
//        fun hideRecyclerView(recyclerView:RecyclerView, loadingErrorLayout:FrameLayout,isLoading:Boolean,errorMsg:String){
//            recyclerView.visibility = View.GONE
//            loadingErrorLayout.visibility = View.VISIBLE
//            var loader = loadingErrorLayout.findViewById<ContentLoadingProgressBar>(R.id.contentLoadingProgressBar)
//            var errorMsgLinearLayout = loadingErrorLayout.findViewById<LinearLayout>(R.id.errorMsgLinearLayout)
//            if(isLoading){
//                loader.visibility = View.VISIBLE
//                errorMsgLinearLayout.visibility = View.GONE
//            }else{
//                loader.visibility = View.GONE
//                errorMsgLinearLayout.visibility = View.VISIBLE
//                var errorMsgText = loadingErrorLayout.findViewById<TextView>(R.id.errorMsgText)
//                errorMsgText.setText(errorMsg)
//            }
//        }
        fun setRecyclerViewItemSpacing(recyclerView:RecyclerView){
            val spacingDecorator = SpacingItemDecoration(30)
            recyclerView.addItemDecoration(spacingDecorator)
        }




    }
}