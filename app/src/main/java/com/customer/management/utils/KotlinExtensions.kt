package com.customer.management.utils

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.customer.management.utils.KotlinExtensions.Companion.onChange

class KotlinExtensions {
    companion object {
        fun EditText.onChange(cb: (CharSequence?,Int) -> Unit) {
            this.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { cb(s,count)
                }
            })
        }
    }
}

