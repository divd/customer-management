package com.customer.management

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.customer.management.utils.Utils

open class BaseActivity : AppCompatActivity() {
    var context: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try{
            context = this
        } catch (e: Exception){
            Utils.logException(BaseActivity::class.java, "onCreate", e)
        }
    }
}