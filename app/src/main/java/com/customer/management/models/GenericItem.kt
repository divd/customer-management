package com.customer.management.models

data class GenericItem(
    var item_id: String,
    var item_image_url: String,
    var item_name_english: String,
    var item_name_hindi: String,
    var item_name_marathi: String,
    var item_sub_category: String,
    var item_base_quantity:String,
    var item_is_selected:Boolean = false,
    var item_price: String = "",
    var operation_type:String = ""
)