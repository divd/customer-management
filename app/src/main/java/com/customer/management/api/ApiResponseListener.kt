package com.customer.management.api

import org.json.JSONObject

interface ApiResponseListener {
    fun getSuccessResponse(responseObject: JSONObject)
    fun getErrorResponse(errorCode:Int,errorMsg:String)
}