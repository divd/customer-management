package com.customer.management.api

import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject


class ApiSingleton private constructor() {
    private val TAG = "ApiSingleton"
    private val prefixURL = "http://dsfs"
    var requestQueue: RequestQueue? = null

    fun apiRequestPost(param1: Any?, listener: ApiResponseListener) {
        val url: String = prefixURL + "suffix"

        val jsonParams: MutableMap<String?, Any?> = HashMap()
        jsonParams["param1"] = param1

        val request =
            JsonObjectRequest(
                Request.Method.POST, url, JSONObject(jsonParams),
                Response.Listener { response ->
                    Log.d("$TAG: ", "Response : $response")
                    if (null != response) listener.getSuccessResponse(response)
                },
                Response.ErrorListener { error ->
                    if (null != error.networkResponse) {
                        Log.d(
                            "$TAG: ",
                            "Error Response code: " + error.networkResponse.statusCode
                        )
                        listener.getSuccessResponse(JSONObject())
                    }
                })
        requestQueue?.add(request)
    }

    private val instance: ApiSingleton? = null

    private object HOLDER {
        val INSTANCE = ApiSingleton()
    }

    companion object {
        val instance: ApiSingleton by lazy { HOLDER.INSTANCE }
    }
}