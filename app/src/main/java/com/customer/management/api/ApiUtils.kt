package com.customer.management.api

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.customer.management.R
import org.json.JSONObject


class ApiUtils {
    companion object{
        fun makeApiCall(context: Context?, requestObject: JSONObject?,url:String, responseListener: ApiResponseListener) {
            //pre processing
            apiRequestPost(
                context,
                requestObject,
                url,
                responseListener
            )
        }

        private fun apiRequestPost(context: Context?, param1: JSONObject?,url:String, listener: ApiResponseListener) {
            val TAG = "ApiSingleton"
            val prefixURL ="http://maximatech.in/customermanagement/"
            var requestQueue: RequestQueue? = Volley.newRequestQueue(context)


            val fullUrl: String = prefixURL + url

//            val jsonParams: MutableMap<String?, Any?> = HashMap()
//            jsonParams["param1"] = param1

            val request =
                JsonObjectRequest(
                    Request.Method.POST, fullUrl, param1,
                    Response.Listener { response ->
                        Log.d("$TAG: ", "Response : $response")
                        if (null != response) listener.getSuccessResponse(response)
                    },
                    Response.ErrorListener { error ->
                        if (null != error.networkResponse) {
                            Log.d("$TAG: ", "Error Response code: " + error.networkResponse.statusCode)
                            if (error is TimeoutError || error is NoConnectionError) {
                                listener.getErrorResponse(error.networkResponse.statusCode,"No Internet Connection")
                            } else if (error is AuthFailureError) {
                                listener.getErrorResponse(error.networkResponse.statusCode,"Authentication Error")
                            } else if (error is ServerError) {
                                listener.getErrorResponse(error.networkResponse.statusCode,"Server Error")
                            } else if (error is NetworkError) {
                                listener.getErrorResponse(error.networkResponse.statusCode,"Internet Connection Error")
                            } else if (error is ParseError) {
                                listener.getErrorResponse(error.networkResponse.statusCode,"Something went Wrong")
                            }else{
                                listener.getErrorResponse(error.networkResponse.statusCode,"Something went Wrong")
                            }

                        }
                    })
            requestQueue?.add(request)
        }
    }
}