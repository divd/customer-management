package com.customer.management.activities

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.ktx.Firebase
import com.customer.management.R
import com.customer.management.utils.KotlinExtensions.Companion.onChange
import com.customer.management.utils.Utils.Companion.hideKeyboard
import com.customer.management.utils.Utils.Companion.isValidPhoneNumber
import com.customer.management.utils.Utils.Companion.makeDebugToast
import java.util.concurrent.TimeUnit


class LoginScreen : AppCompatActivity() {
    val TAG = "MyMessage"
    lateinit var proceedBtn: Button
    lateinit var nameEditText: TextInputLayout
    lateinit var phoneEditText: TextInputLayout

    lateinit var otp1: EditText
    lateinit var otp2: EditText
    lateinit var otp3: EditText
    lateinit var otp4: EditText
    lateinit var otp5: EditText
    lateinit var otp6: EditText
    lateinit var resendOTP:TextView
    lateinit var otpErrorText:TextView

    lateinit var nameNoLayout: LinearLayout
    lateinit var otpLayout: LinearLayout

    lateinit var mAuth: PhoneAuthProvider
    lateinit var auth: FirebaseAuth
     lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    lateinit var storedVerificationId: String
    lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    var isOTPVisible = false

    lateinit var sharedPreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_screen)

        sharedPreferences = applicationContext.getSharedPreferences("customer_details",Context.MODE_PRIVATE)

        if(sharedPreferences.getBoolean("isLoggedIn",false)){
//            val intent = Intent(this, GenericItemSelectionList::class.java)
//            startActivity(intent)
//            finish()
        }
         proceedBtn = findViewById(R.id.proceedBtn)
         nameEditText = findViewById(R.id.userName)
         phoneEditText = findViewById(R.id.phoneNo)
         nameNoLayout = findViewById(R.id.nameNumberLayout)
         otpLayout = findViewById(R.id.otpLayout)

        otp1 = findViewById(R.id.otp1)
        otp2 = findViewById(R.id.otp2)
        otp3 = findViewById(R.id.otp3)
        otp4 = findViewById(R.id.otp4)
        otp5 = findViewById(R.id.otp5)
        otp6 = findViewById(R.id.otp6)

        resendOTP = findViewById(R.id.resendOTP)
        otpErrorText = findViewById(R.id.otpErrorText)
        auth = Firebase.auth
        mAuth = PhoneAuthProvider.getInstance()


        proceedBtn.setOnClickListener {
            if(!isOTPVisible) {
                nameEditText.error = null
                phoneEditText.error = null
                if(nameEditText.editText?.text.toString()==""){
                    nameEditText.error = getString(R.string.empty_name_error)
                    //makeToast(applicationContext,getString(R.string.empty_name_error))
                    return@setOnClickListener
                }
                if(phoneEditText.editText?.text.toString()==""){
                    phoneEditText.error = getString(R.string.empty_number_error)
                   // makeToast(applicationContext,getString(R.string.empty_number_error))
                    return@setOnClickListener
                }
                if(!isValidPhoneNumber(phoneEditText.editText?.text.toString()) || phoneEditText.editText?.text.toString().length!=10){
                    phoneEditText.error = getString(R.string.invalid_number_error)
                  //  makeToast(applicationContext,getString(R.string.invalid_number_error))
                    return@setOnClickListener
                }

                sendOTP(phoneEditText.editText?.text.toString())


            }else{
                otpErrorText.visibility = View.INVISIBLE
                if(otp1.text.toString()=="" || otp2.text.toString()=="" ||otp3.text.toString()=="" ||otp4.text.toString()=="" ||otp5.text.toString()=="" ||otp6.text.toString()=="" ){
                    otpErrorText.visibility = View.VISIBLE
                    otpErrorText.text = getString(R.string.empty_otp_error)
                    //  makeToast(applicationContext,getString(R.string.empty_otp_error))
                    return@setOnClickListener
                }
                val fullOTP = otp1.text.toString()+otp2.text.toString()+otp3.text.toString()+otp4.text.toString()+otp5.text.toString()+otp6.text.toString()
                if(fullOTP.length!=6){
                    otpErrorText.visibility = View.VISIBLE
                    otpErrorText.text = getString(R.string.invalid_otp_error)
                   // makeToast(applicationContext,getString(R.string.invalid_otp_error))
                    return@setOnClickListener
                }
                val credential = PhoneAuthProvider.getCredential(storedVerificationId, fullOTP)
                signInWithPhoneAuthCredential(credential)
            }
        }
        resendOTP.setOnClickListener{
            resendOTP(phoneEditText.editText?.text.toString())
        }
    }



    fun moveOTPFocus(){
        otp1.onChange {s,count-> if(count!=0){ otp2.requestFocus()} }
        otp2.onChange {s,count-> if(count!=0){ otp3.requestFocus()}else{otp1.requestFocus()} }
        otp3.onChange {s,count-> if(count!=0){ otp4.requestFocus()}else{otp2.requestFocus()} }
        otp4.onChange {s,count-> if(count!=0){ otp5.requestFocus()}else{otp3.requestFocus()} }
        otp5.onChange {s,count-> if(count!=0){ otp6.requestFocus()}else{otp4.requestFocus()} }
        otp6.onChange {s,count-> if(count!=0){otp6.hideKeyboard()}else{otp5.requestFocus()} }
    }

    private fun sendOTP(phoneNo: String){
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:$credential")
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e)
                makeDebugToast(applicationContext,"onVerificationFailed$e")
                if (e is FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }
                // Show a message and update the UI
                // ...
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                showOTPUi()
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:$verificationId")
                // Save verification ID and resending token so we can use them later
                storedVerificationId = verificationId
                resendToken = token
                // ...
            }
        }
        mAuth.verifyPhoneNumber(
            "+91$phoneNo", // Phone number to verify
            60, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            this, // Activity (for callback binding)
            callbacks) // OnVerificationStateChangedCallbacks
    }

    private fun resendOTP(phoneNo: String){
        mAuth.verifyPhoneNumber(
            "+91$phoneNo", // Phone number to verify
            60, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            this, // Activity (for callback binding)
            callbacks,
            resendToken
        ) // OnVerificationStateChangedCallbacks
    }



          fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
             auth.signInWithCredential(credential)
                 .addOnCompleteListener(this) { task ->
                     if (task.isSuccessful) {

                         //test api call
                         FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) { instanceIdResult: InstanceIdResult ->
                             val newToken = instanceIdResult.token
                             Log.e("newToken", newToken)
                             var grocery =  "grocery"

                             makeDebugToast(applicationContext,"login success")

//                             val requestObject = JSONObject()
//                             requestObject.put("reg_mobile_no",phoneEditText.editText?.text.toString())
//                             requestObject.put("fcm_token",newToken)
//                             requestObject.put("vendor_name",nameEditText.editText?.text.toString())
//                             requestObject.put("vendor_category",grocery)
//                             ApiUtils.makeApiCall(
//                                 applicationContext,
//                                 requestObject,
//                                 "vendor/apiUpdateVendorDetails.php",
//                                 object : ApiResponseListener {
//                                     override fun getSuccessResponse(responseObject: JSONObject) {
//                                         Log.d(TAG, responseObject.toString())
//
//
//                                         val editor:SharedPreferences.Editor =  sharedPreferences.edit()
//                                         editor.putString("vendor_name",nameEditText.editText?.text.toString())
//                                         editor.putBoolean("isLoggedIn",true)
//                                         editor.putString("vendor_code",responseObject.getString("vendor_code"))
//                                         editor.apply()
//
////                                         val intent = Intent(applicationContext, GenericItemSelectionList::class.java)
////                                         startActivity(intent)
////                                         finish()
//                                         //   Toast.makeText(context,"Api call", Toast.LENGTH_SHORT).show()
//                                         // Sign in success, update UI with the signed-in user's information
//                                         Log.d(TAG, "signInWithCredential:success")
//                                         //makeDebugToast(applicationContext,"signInWithCredential:success")
//                                         val user = task.result?.user
//                                     }
//
//                                     override fun getErrorResponse(errorCode: Int, errorMsg: String
//                                     ) {
//                                         TODO("Not yet implemented")
//                                     }
//                                 })
                         }




                         // ...
                     } else {
                         // Sign in failed, display a message and update the UI
                         Log.w(TAG, "signInWithCredential:failure", task.exception)
                        // makeDebugToast(applicationContext,"signInWithCredential:failure")
                         if (task.exception is FirebaseAuthInvalidCredentialsException) {
                             // The verification code entered was invalid
                             otpErrorText.visibility = View.VISIBLE
                             otpErrorText.text = getString(R.string.otp_mismatch)
                         }
                     }
                 }
         }

    fun showOTPUi(){
        val animFadeout = AnimationUtils.loadAnimation(applicationContext,
            R.anim.fade_out
        )
        nameNoLayout.startAnimation(animFadeout)
        animFadeout.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {}
            override fun onAnimationRepeat(p0: Animation?) {}
            override fun onAnimationEnd(p0: Animation?) {
                nameNoLayout.visibility = View.INVISIBLE
            }
        })
        val animFadeIn = AnimationUtils.loadAnimation(applicationContext,
            R.anim.fade_in
        )
        otpLayout.startAnimation(animFadeIn)
        animFadeIn.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {}
            override fun onAnimationRepeat(p0: Animation?) {}
            override fun onAnimationEnd(p0: Animation?) {
                isOTPVisible = true
                otpLayout.visibility = View.VISIBLE
                moveOTPFocus()
            }
        })
    }


    override fun onBackPressed() {
        if(isOTPVisible) {
            val animFadeout = AnimationUtils.loadAnimation(applicationContext,
                R.anim.fade_out
            )
            otpLayout.startAnimation(animFadeout)
            animFadeout.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(p0: Animation?) {}
                override fun onAnimationRepeat(p0: Animation?) {}
                override fun onAnimationEnd(p0: Animation?) {
                    isOTPVisible = false
                    otpLayout.visibility = View.INVISIBLE
                }
            })
            val animFadeIn = AnimationUtils.loadAnimation(applicationContext,
                R.anim.fade_in
            )
            nameNoLayout.startAnimation(animFadeIn)
            animFadeIn.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(p0: Animation?) {}
                override fun onAnimationRepeat(p0: Animation?) {}
                override fun onAnimationEnd(p0: Animation?) {
                    nameNoLayout.visibility = View.VISIBLE
                }
            })
        }else{
            super.onBackPressed()
        }
    }
}