package com.customer.management

import android.app.Application
import android.content.Context
import com.mikepenz.iconics.Iconics


class ApplicationClass : Application() {

    companion object {
        var ctx: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        ctx = applicationContext
        Iconics.init(applicationContext)
    }

}