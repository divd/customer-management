package com.customer.management.interfaces

interface GenericItemClickListner {
    fun onUserClick()
}